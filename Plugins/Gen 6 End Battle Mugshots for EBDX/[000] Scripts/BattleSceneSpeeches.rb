class PokeBattle_Scene
    #-----------------------------------------------------------------------------
    # function to process actual speech
    #-----------------------------------------------------------------------------
    def pbTrainerSpeak(msg, bgm = nil, index = 0)
      # in case the dialogue is a hash for bgm change as well as text display
      file = (msg.is_a?(Hash) && msg.has_key?(:file)) ? msg[:file] : nil
      bgm = msg[:bgm] if msg.is_a?(Hash) && msg.has_key?(:bgm)
      msg = msg[:text] if msg.is_a?(Hash) && msg.has_key?(:text)
      # play specified BGM
      pbBGMPlay(bgm) if !bgm.nil?
      self.custom_bgm = bgm if !bgm.nil?
      # safety check
      if msg.is_a?(Proc)
        wrapper = CallbackWrapper.new
        wrapper.set({
          :battle => @battle, :scene => self, :sprites => @sprites,
          :battlers => @battle.battlers, :opponent => @battle.opponent,
          :viewport => @viewport, :vector => self.vector
        })
        return wrapper.execute(msg)
      end
      return pbBattleCommonEvent(msg) if msg.is_a?(Integer)
      return if !msg.is_a?(String) && !msg.is_a?(Array)
      msg = [msg] if !msg.is_a?(Array)
      # show opponent Trainer on screen
      clearMessageWindow
      pbShowOpponent(index, true, file) if (@battle.opponent && @battle.opponent[index]) || !file.nil?
      # display message
      for m in msg
        @battle.pbDisplayPaused(m)
      end
      clearMessageWindow
      # hide opponent Trainer off screen
      pbHideOpponent(true, true) if (@battle.opponent && @battle.opponent[index]) || !file.nil?
    end
    #-----------------------------------------------------------------------------
    #  visuals to show opponent in scene
    #-----------------------------------------------------------------------------
    alias pbShowOpponent_g6 pbShowOpponent unless self.method_defined?(:pbShowOpponent_g6)
    def pbShowOpponent(index = 0, speech = false, file = nil)
      return pbShowOpponent_g6(index,speech,file) if (!EliteBattle::ENABLE_ENDSPEECH_MUGSHOT)
      return unless (@battle.opponent && @battle.opponent[index]) || !file.nil?
      pbSetMessageMode(false, true)
      trainerid = @battle.opponent[index].trainer_type
      # hide databoxes
      pbHideAllDataboxes
      # hide old trainer first if necessary
      pbHideOpponent if @sprites["opponent"]
      # draws opponent sprite
      @sprites["opponent"]=Sprite.new(@viewport)
      @sprites["opponent"].bitmap=pbBitmap("Graphics/Trainers/Mugshots/t#{trainerid}")#Bitmap.new(@viewport.rect.width,@viewport.rect.height)
      @sprites["opponent"].ox = @sprites["opponent"].bitmap.width/2
      @sprites["opponent"].oy = @sprites["opponent"].bitmap.height
      @sprites["opponent"].x = Graphics.width
      @sprites["opponent"].y = Graphics.height
      @sprites["opponent"].zoom_x = 1
      @sprites["opponent"].zoom_y = 1
      @sprites["opponent"].z=99998
      @sprites["opponent"].opacity=0

      trainer = (@battle.opponent && @battle.opponent[index]) ? @battle.opponent[index] : nil
      v = (@battle.doublebattle? && speech) ? 3 : -1
      ox = @sprites["battlebg"].battler(v).x + (speech ? 200 : 160) + (@battle.doublebattle? && speech ? 96 : 0)
      oy = @sprites["battlebg"].battler(v).y - (speech ? 2 : -40)

      # draws black boxes
      @sprites["box1"] = Sprite.new(@viewport)
      @sprites["box1"].z = 99999
      @sprites["box1"].create_rect(2, 32 ,Color.black)
      @sprites["box1"].zoom_x = 0
      @sprites["box1"].x = -2
      @sprites["box2"] = Sprite.new(@viewport)
      @sprites["box2"].z = 99999
      @sprites["box2"].create_rect(2, 32, Color.black)
      @sprites["box2"].zoom_x = 0
      @sprites["box2"].ox = 2
      @sprites["box2"].x = @viewport.width + 2
      @sprites["box2"].y = @viewport.height - 32
      x = v > 0 ? -8 : -6
      for i in 0...20.delta_add
        k = 20.delta_add/20.0
        moveEntireScene(x, (speech ? +2 : -1), true, true) if i%k > 0 || k == 1
        @sprites["opponent"].opacity += 12.8.delta_sub(false)
        @sprites["opponent"].x -= (Graphics.width/40) if i%k > 0 || k == 1 #x if i%k > 0 || k == 1
        #@sprites["opponent"].y += (speech ? 2 : -1) if i%k > 0 || k == 1
        @sprites["box1"].zoom_x += (@viewport.width/16).delta_sub(false) if speech
        @sprites["box2"].zoom_x += (@viewport.width/16).delta_sub(false) if speech
        self.wait(1, true)
      end
      @sprites["opponent"].opacity = 255
      @sprites["opponent"].x = 256
      #@sprites["opponent"].y = oy + (speech ? 2 : -1)*20
      @sprites["box1"].zoom_x = @viewport.width if speech
      @sprites["box2"].zoom_x = @viewport.width if speech
    end
    #-----------------------------------------------------------------------------
    #  visuals to hide opponent in scene
    #-----------------------------------------------------------------------------
    alias pbHideOpponent_g6 pbHideOpponent unless self.method_defined?(:pbHideOpponent_g6)
    def pbHideOpponent(showboxes = false, speech = false)
      return pbHideOpponent_g6(showboxes, speech) if (!EliteBattle::ENABLE_ENDSPEECH_MUGSHOT)
      return if !@sprites["opponent"] || @sprites["opponent"].disposed?
      pbSetMessageMode(false)
      v = (@battle.doublebattle? && speech) ? 3 : -1
      x = v > 0 ? 8 : 6
      for i in 0...20.delta_add
        k = 20.delta_add/20.0
        moveEntireScene(x, (speech ? -2 : +1), true, true) if i%k > 0 || k == 1
        @sprites["opponent"].opacity -= 12.8.delta_sub(false)
        @sprites["opponent"].x += (Graphics.width/40) if i%k > 0 || k == 1#x if i%k > 0 || k == 1
        @sprites["box1"].zoom_x -= (@viewport.width/16).delta_sub(false) if speech
        @sprites["box2"].zoom_x -= (@viewport.width/16).delta_sub(false) if speech
        self.wait(1, true)
      end
      @sprites["opponent"].opacity = 0
      @sprites["box1"].zoom_x = 0 if speech
      @sprites["box2"].zoom_x = 0 if speech
      # show databoxes
      pbShowAllDataboxes
      # dispose sprites
      @sprites["opponent"].dispose
      @sprites["box1"].dispose
      @sprites["box2"].dispose
    end
end